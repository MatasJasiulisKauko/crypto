<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

define('TEMPLATES_DIR', __DIR__.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR);
define('LIBRARIES_DIR', __DIR__.DIRECTORY_SEPARATOR.'libraries'.DIRECTORY_SEPARATOR);
define('CONTROLLERS_DIR', __DIR__.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR);
define('MODELS_DIR', __DIR__.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR);
define('CONFIGS_DIR', __DIR__.DIRECTORY_SEPARATOR.'configs'.DIRECTORY_SEPARATOR);

$defaultController = 'default';
$defaultMethod = 'index';

$route = explode('/', $_SERVER['REQUEST_URI']);

if (isset($route[2]))
{
    $defaultController = $route[2];
}

if (isset($route[3]))
{
    $defaultMethod = $route[3];
}

$controllerName = ucfirst($defaultController).'Controller';
include_once (CONTROLLERS_DIR.$controllerName.'.php');
include_once (LIBRARIES_DIR.'view.php');
include_once (LIBRARIES_DIR.'config.php');
include_once (LIBRARIES_DIR.'db.php');

$controller = new $controllerName();
$controller->{$defaultMethod.'Action'}();







