<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <style type="text/css">

        #text {
            height: 25px;
            border-radius: 5px;
            padding: 4px;
            border: solid thin #aaa;
            width: 100%;
        }

        #button {
            padding: 10px;
            width: 100px;
            color: white;
            background-color: lightblue;
            border: none;
        }

        #box {
            background-color: grey;
            margin: auto;
            width: 300px;
            padding: 20px;
        }

    </style>
</head>
<body>

<div id="box">

    <h1>Default puslapis</h1>
    <h2>Sveiki, <?= $name ?></h2>
    <h3>Šiandien yra: <?= $date ?></h3>

    <?php if (isset($_SESSION['user'])): ?>
        <a href="index.php/logout">Logout</a>
    <?php else: ?>
        <a href="index.php/login">Login</a>
    <?php endif; ?>
</div>

<canvas id="myChart" width="200" height="200"></canvas>

</body>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>
<script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["<?= implode('","', array_keys($btcData)) ?>"],
            datasets: [{
                label: '# of Votes',
                data: [<?= implode(',', $btcData) ?>],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
</script>
</html>