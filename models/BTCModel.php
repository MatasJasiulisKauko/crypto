<?php


class BTCModel
{
    public function getBtcData()
    {
        $dbData = db::query("SELECT transaction_date, amount FROM btc");

        $btcData = [];
        foreach ($dbData as $btcItem) {
            $btcData[$btcItem['transaction_date']] = $btcItem['amount'];
        }

        return $btcData;
    }
}