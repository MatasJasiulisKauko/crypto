<?php

class LoginController
{
    /**
     * To show login window
     */
    public function indexAction()
    {
        //1. apdoroti requesta
        //2. pagal requesta padarome duomenis
        //3. atiduodam views'a

        $params = [
            'errorMsg' => '',
        ];

        //todo: session handler with flashbag
        if (isset($_SESSION['errorMsg'])) {
            $params['errorMsg'] = $_SESSION['errorMsg'];
            unset($_SESSION['errorMsg']);
        }

        $view = new view();
        $view->render('login', $params);
    }

    /**
     * Execute user Login
     */
    public function loginAction()
    {
        if (empty($_POST)) {
            //TODO: write routing class, with redirect availability
            //$router->redirect('login');
            die('ne cia pataikei xaxaxa!');
        }

        if (!empty($_POST['user_name']) && !empty($_POST['password']))
        {
            $user = db::queryOneRow(
                "SELECT * FROM users WHERE email = ? LIMIT 1",
                [
                    $_POST['user_name']
                ]
            );

            if (md5($_POST['password']) === ($user['password']))
            {
                $_SESSION['user'] = [
                    'email' => $_POST['user_name'],
                ];

                //TODO: write routing class, with redirect availability
                //$router->redirect('index');
                header('Location: http://crypto.local/index.php');

                exit;
            } else {
                $_SESSION['errorMsg'] = 'Vartotojo vardas arba Slaptažodis yra neteisingi!';
                header('Location: http://crypto.local/index.php/login');

                exit;
            }
        }

    }
}


