<?php

class LogoutController
{
    /**
     * To logout user
     */
    public function indexAction()
    {
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
        }

        header('Location: http://crypto.local/index.php');

        exit;
    }
}


