<?php

class DefaultController
{
    public function indexAction()
    {
        //1. apdoroti requesta
        //2. pagal requesta padarome duomenis
        //3. atiduodam views'a

        include_once MODELS_DIR.'BTCModel.php';

        $btcModel = new BTCModel();
        $btcData = $btcModel->getBtcData();

        $view = new view();

        $view->render('default', [
            'name' => isset($_SESSION['user']) ? $_SESSION['user']['email'] : 'Anonimas',
            'btcData' => $btcData,
            'date' => date("Y-m-d H:i:s"),
        ]);
    }
}
