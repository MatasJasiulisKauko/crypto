<?php

class view
{
    public function render(string $templateName, array $parameters = [])
    {
        $file = TEMPLATES_DIR.$templateName.'.php';

        if (is_file($file))
        {
            ob_start();
            extract($parameters, EXTR_SKIP);
            include $file;
            $result = ob_get_contents();
            ob_end_clean();
            echo $result;
        }
    }
}