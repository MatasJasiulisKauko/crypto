<?php

class db
{
    private static $connection = null;

    private static function connect()
    {
        $config = new config();
        $dbConfig = $config->get('database');

        try {
            self::$connection = new PDO(
                "mysql:host=".$dbConfig['servername'].";dbname=".$dbConfig['db'],
                $dbConfig['username'],
                $dbConfig['password']
            );

            // set the PDO error mode to exception
            self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();

            exit;
        }
    }

    public static function query(string $sql, array $parameters = [])
    {
        $stmt = self::getStatement($sql, $parameters);

        return $stmt->fetchAll();
    }

    public static function queryOneRow(string $sql, array $parameters = [])
    {
        $stmt = self::getStatement($sql, $parameters);

        return $stmt->fetch();
    }

    private static function getStatement(string $sql, array $parameters = [])
    {
        if (self::$connection === null) {
            self::connect();
        }

        $stmt = self::$connection->prepare($sql);
        $stmt->execute($parameters);

        return $stmt;
    }
}